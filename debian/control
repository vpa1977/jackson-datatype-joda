Source: jackson-datatype-joda
Section: java
Priority: optional
Maintainer: Debian Java maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Tim Potter <tpot@hp.com>
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 javahelper,
 junit4,
 libbuild-helper-maven-plugin-java,
 libjackson2-annotations-java (>= 2.8.5),
 libjackson2-core-java (>= 2.8.5),
 libjackson2-databind-java (>= 2.8.5),
 libjoda-time-java,
 libmaven-bundle-plugin-java,
 libmaven-enforcer-plugin-java,
 libmaven-shade-plugin-java,
 libreplacer-java,
 maven-debian-helper,
 xmlstarlet
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/java-team/jackson-datatype-joda.git
Vcs-Browser: https://salsa.debian.org/java-team/jackson-datatype-joda
Homepage: https://github.com/FasterXML/jackson-datatype-joda

Package: libjackson2-datatype-joda-java
Architecture: all
Depends: ${misc:Depends}, ${maven:Depends}
Suggests: ${maven:OptionalDepends}
Description: fast and powerful JSON library for Java -- Joda datatypes
 The Jackson Data Processor is a multi-purpose Java library for processing
 JSON. Jackson aims to be the best possible combination of fast, correct,
 lightweight, and ergonomic for developers. It offers three alternative methods
 for processing JSON:
 .
  * Streaming API inspired by StAX
  * Tree Model
  * Data Binding converts JSON to and from POJOs
 .
 In addition to the core library, there are numerous extension that provide
 additional functionality such as additional data formats beyond JSON,
 additional data types or JVM languages.
 .
 This package contains an extension for serializing and deserializing classes
 implemented by the Joda-Time date/time library.
